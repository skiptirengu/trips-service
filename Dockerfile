FROM php:7.1.2-apache

RUN apt-get update \
    && apt-get -y install \
            libicu52 \
            libicu-dev \

            # Required by composer
            git \
            zlib1g-dev \
        --no-install-recommends \

    # Required extension
    && docker-php-ext-install -j$(nproc) intl \

    # Additional common extensions
    && docker-php-ext-install -j$(nproc) opcache \
    && docker-php-ext-install pdo pdo_mysql \

    # Required by composer
    && docker-php-ext-install -j$(nproc) zip \

    # Cleanup to keep the images size small
    && apt-get purge -y \
        icu-devtools \
        libicu-dev \
        zlib1g-dev \
    && apt-get autoremove -y \
    && rm -r /var/lib/apt/lists/*

# Add Yii2 config
COPY ./docker/docker-yii2-php.conf /etc/apache2/conf-available/
RUN sed -i 's#DocumentRoot.*#DocumentRoot /var/www/html/web#' /etc/apache2/sites-available/000-default.conf \
    && sed -i 's/^#AddDefault/AddDefault/' /etc/apache2/conf-available/charset.conf \
    && sed -i 's/ServerTokens OS/ServerTokens Prod/' /etc/apache2/conf-available/security.conf \
    && sed -i 's/ServerSignature On/ServerSignature Off/' /etc/apache2/conf-available/security.conf \
    && a2disconf docker-php \
    && a2enconf docker-yii2-php \
    && a2enmod rewrite

# Change to app dir
WORKDIR /var/www/html

# Install composer
COPY ./docker/install-composer /install-composer
RUN chmod +x /install-composer
RUN bash /install-composer && rm /install-composer

# Install deps
COPY ./composer.json ./
COPY ./composer.lock ./
RUN composer install \
    --no-scripts \
    --no-autoloader \
    --no-dev

# Add project and generate autoloader
COPY ./ ./
COPY ./docker/local-bootstrap.php ./config
RUN composer dump-autoload --optimize

# Runtime directories
RUN mkdir web/assets runtime
RUN chmod 777 -R web/ runtime/

EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]