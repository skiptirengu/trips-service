# Trips Service - Dynamox backend apply

Simple rest service built with PHP using Yii2 framework and MySQL.

## Running

The service has a built-in docker configuration. Simply run

```bash
docker-compose build 
```
then 

```bash
docker-compose up -d
```

Once both containers are running, login to the "web" container in order to run the database migrations
```bash
docker exec -i -t tripsclient_web_1 bash
```
then 

```bash
php yii migrate
```

After this you can access the service on the port `8080`.

## Unit tests

Make sure your dependencies are up to date running

```bash
composer install
```
then run the tests with

```bash
vendor/bin/phpunit
```