<?php

namespace app\auth;

use yii\base\Object;
use yii\web\IdentityInterface;

/**
 * Identity class
 */
class User extends Object implements IdentityInterface
{
    /**
     * @var array
     */
    private static $users = [
        '42' => [
            'id' => '42',
            'username' => 'TestUser',
            'password' => 'admin',
            'authKey' => 'user42key',
            'accessToken' => 'user42token',
        ],
        '66' => [
            'id' => '66',
            'username' => 'NewUser',
            'password' => 'password',
            'authKey' => 'user66key',
            'accessToken' => 'user66token',
        ],
    ];

    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $username;
    /**
     * @var string
     */
    public $password;
    /**
     * @var string
     */
    public $authKey;
    /**
     * @var string
     */
    public $accessToken;

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByUserAndPassword($username, $password)
    {
        foreach (self::$users as $user) {
            if ($user['username'] === $username && $user['password'] === $password) {
                return new static($user);
            }
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
}