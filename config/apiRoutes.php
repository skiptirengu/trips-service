<?php

return [
    'POST v1/places/<placeId:\d+>/features' => 'v1/features/create',
    [
        'class' => yii\rest\UrlRule::class,
        'pluralize' => false,
        'controller' => [
            'v1/companies',
            'v1/customers',
            'v1/places',
            'v1/trips'
        ]
    ],
    [
        'class' => yii\rest\UrlRule::class,
        'pluralize' => false,
        'controller' => 'v1/features',
        'patterns' => [
            'PUT,PATCH {id}' => 'update',
            'DELETE {id}' => 'delete',
            'GET,HEAD {id}' => 'view',
            'GET,HEAD' => 'index',
            '{id}' => 'options',
            '' => 'options',
        ]
    ]
];