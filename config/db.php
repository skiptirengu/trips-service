<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=trips_service',
    'username' => 'dbuser',
    'password' => 'newpassword',
    'charset' => 'utf8',
];
