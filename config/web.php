<?php

$config = [
    'id' => 'trips-service',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => app\modules\v1\Module::class
    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'G9Dz-pKc1oMPU3nJH6clGINlT0r0pZQQ',
            'parsers' => [
                'application/json' => yii\web\JsonParser::class,
            ]
        ],
        'user' => [
            'identityClass' => app\auth\User::class,
            'enableSession' => false,
            'loginUrl' => null
        ],
        'cache' => [
            'class' => yii\caching\FileCache::class,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => yii\log\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => require(__DIR__ . '/apiRoutes.php')
        ],
    ],
];

return $config;
