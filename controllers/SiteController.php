<?php

namespace app\controllers;

use Yii;
use yii\base\UserException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    /**
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        throw new NotFoundHttpException('Not Found');
    }

    /**
     * @return string
     */
    public function actionError()
    {
        if (($exception = Yii::$app->getErrorHandler()->exception) === null) {
            $exception = new NotFoundHttpException('Not Found');
        }

        Yii::$app->getResponse()->setStatusCodeByException($exception);

        return $exception instanceof UserException
            ? $exception->getMessage()
            : 'An internal server error occurred.';
    }
}
