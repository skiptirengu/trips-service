<?php

define('YII_DEBUG', false);
define('YII_ENV', 'prod');

return [
    'components' => [
        'db' => ['dsn' => 'mysql:host=db;dbname=trips_service']
    ]
];