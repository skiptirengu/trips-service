<?php

use yii\db\Migration;

class m170619_183415_init_db extends Migration
{
    public function safeUp()
    {
        $this->createTable('customers', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'email' => $this->string()->notNull()->unique()
        ]);

        $this->createTable('places', [
            'id' => $this->primaryKey(),
            'label' => $this->string()->notNull(),
        ]);

        $this->createTable('features', [
            'id' => $this->primaryKey(),
            'type' => $this->string()->notNull(),
            'place_id' => $this->integer()->notNull(),
            'properties' => $this->text()->null(),
            'latitude' => $this->double()->notNull(),
            'longitude' => $this->double()->notNull()
        ]);
        $this->addForeignKey(
            'fk_place_features',
            'features',
            'place_id',
            'places',
            'id',
            'CASCADE', 'CASCADE'
        );

        $this->createTable('companies', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()
        ]);

        $this->createTable('trips', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->null(),
            'from' => $this->integer()->null(),
            'target' => $this->integer()->null(),
            'departure' => $this->dateTime()->notNull(),
            'return' => $this->dateTime()->notNull(),
            'cost' => $this->double()->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
            'customer_id' => $this->integer()->null()
        ]);
        $this->addForeignKey(
            'fk_trip_company',
            'trips',
            'company_id',
            'companies',
            'id',
            'SET NULL', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_trip_from',
            'trips',
            'from',
            'places',
            'id',
            'SET NULL', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_trip_target',
            'trips',
            'target',
            'places',
            'id',
            'SET NULL', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_trip_customer',
            'trips',
            'customer_id',
            'customers',
            'id',
            'SET NULL', 'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropTable('trips');
        $this->dropTable('companies');
        $this->dropTable('features');
        $this->dropTable('places');
        $this->dropTable('customers');
    }
}
