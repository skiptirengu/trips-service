<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "companies".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Trips[] $trips
 */
class Companies extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'companies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['name'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'name' => 'name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrips()
    {
        return $this->hasMany(Trips::className(), ['company_id' => 'id']);
    }
}
