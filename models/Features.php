<?php

namespace app\models;

use yii\base\InvalidParamException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "features".
 *
 * @property integer $id
 * @property string $type
 * @property integer $place_id
 * @property string $properties
 * @property double $latitude
 * @property double $longitude
 *
 * @property Places $place
 */
class Features extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'features';
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->properties = Json::decode($this->properties);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['geometry'], 'safe'],
            [['latitude', 'longitude'], 'required'],
            [['type'], 'required', 'message' => 'geomtry.type cannot be blank.'],
            [['type'], 'string', 'max' => 255],
            [['latitude'], 'number', 'min' => -90, 'max' => 90],
            [['longitude'], 'number', 'min' => -180, 'max' => 180],
            [['properties'], function ($attribute) {
                if (empty($this->properties)) {
                    $this->properties = [];
                }
                try {
                    $this->properties = Json::encode($this->properties, JSON_FORCE_OBJECT);
                } catch (InvalidParamException $ex) {
                    $this->addError($attribute, '{attribute} contains invalid JSON data.');
                }
            }, 'skipOnEmpty' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'place_id' => 'place',
            'id' => 'id',
            'type' => 'type',
            'properties' => 'properties',
            'latitude' => 'latitude',
            'longitude' => 'longitude',
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = parent::fields();

        // set new composite fields
        $fields['type'] = function () {
            return 'Feature';
        };
        $fields['geometry'] = function () {
            return [
                'type' => $this->type,
                'coordinates' => [$this->latitude, $this->longitude]
            ];
        };

        // unset composite fields
        foreach (['place_id', 'latitude', 'longitude', 'feature_id'] as $field) {
            unset($fields[$field]);
        }

        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlace()
    {
        return $this->hasOne(Places::className(), ['id' => 'place_id']);
    }

    /**
     * @param array $params
     * @return void
     */
    public function setGeometry($params = [])
    {
        $this->type = ArrayHelper::getValue($params, 'type');
        list($latitude, $longitude) = ArrayHelper::getValue($params, 'coordinates');
        $this->setAttributes(compact('latitude', 'longitude'), false);
    }
}
