<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "places".
 *
 * @property integer $id
 * @property string $label
 *
 * @property Features[] $features
 */
class Places extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'places';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['label'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = parent::fields();
        $fields['geo'] = function () {
            return [
                'type' => 'FeatureCollection',
                'features' => $this->features
            ];
        };
        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'label' => 'label',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeatures()
    {
        return $this->hasMany(Features::className(), ['place_id' => 'id']);
    }
}
