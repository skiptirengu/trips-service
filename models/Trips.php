<?php

namespace app\models;

use DateTime;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "trips".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $from
 * @property integer $target
 * @property string $departure
 * @property string $return
 * @property double $cost
 * @property string $created_at
 * @property integer $customer_id
 *
 * @property Companies $company
 * @property Customers $customer
 * @property Places $fromPlace
 * @property Places $toTarget
 */
class Trips extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trips';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
                'value' => (new DateTime())->format(DateTime::ISO8601)
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company', 'customer'], 'safe'],
            [['departure', 'return', 'cost', '!company_id', '!customer_id', 'from', 'target'], 'required'],
            [['cost'], 'number', 'min' => 0],
            [
                ['departure', 'return'], 'datetime',
                'format' => 'php:' . DateTime::ISO8601
            ],
            [
                ['!company_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => Companies::className(),
                'targetAttribute' => ['company_id' => 'id']
            ],
            [
                ['!customer_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => Customers::className(),
                'targetAttribute' => ['customer_id' => 'id']
            ],
            [
                ['from'], 'exist',
                'skipOnError' => true,
                'targetClass' => Places::className(),
                'targetAttribute' => ['from' => 'id']
            ],
            [
                ['target'], 'exist',
                'skipOnError' => true,
                'targetClass' => Places::className(),
                'targetAttribute' => ['target' => 'id']
            ],
        ];
    }

    /**
     * @param mixed $val
     * @return void
     */
    public function setCompany($val)
    {
        $this->company_id = $val;
    }

    /**
     * @param mixed $val
     * @return void
     */
    public function setCustomer($val)
    {
        $this->customer_id = $val;
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        return [
            'id',
            'customer',
            'company',
            'from' => 'fromPlace',
            'target' => 'toTarget',
            'cost' => 'cost',
            'createdAt' => 'created_at',
            'departure' => function () {
                return (new DateTime($this->departure))->format(DateTime::ISO8601);
            },
            'return' => function () {
                return (new DateTime($this->departure))->format(DateTime::ISO8601);
            }
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'company_id' => 'company',
            'from' => 'from',
            'target' => 'target',
            'departure' => 'departure',
            'return' => 'return',
            'cost' => 'cost',
            'created_at' => 'created at',
            'customer_id' => 'customer',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromPlace()
    {
        return $this->hasOne(Places::className(), ['id' => 'from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToTarget()
    {
        return $this->hasOne(Places::className(), ['id' => 'target']);
    }
}
