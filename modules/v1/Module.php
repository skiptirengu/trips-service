<?php

namespace app\modules\v1;

use yii\base\Module as BaseModule;

class Module extends BaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerPath = '@app/modules/v1/controllers';
}