<?php

namespace app\modules\v1\controllers;

use app\auth\User;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;

abstract class BaseRestController extends ActiveController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::class,
            'authMethods' => [
                ['class' => HttpBasicAuth::class, 'auth' => [User::class, 'findIdentityByUserAndPassword']],
                ['class' => HttpBearerAuth::class]
            ]
        ];
        return $behaviors;
    }
}