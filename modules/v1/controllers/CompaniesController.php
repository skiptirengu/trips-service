<?php

namespace app\modules\v1\controllers;

use app\models\Companies;

class CompaniesController extends BaseRestController
{
    /**
     * @inheritdoc
     */
    public $modelClass = Companies::class;
}