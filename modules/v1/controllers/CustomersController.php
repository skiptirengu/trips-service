<?php

namespace app\modules\v1\controllers;

use app\models\Customers;

class CustomersController extends BaseRestController
{
    /**
     * @inheritdoc
     */
    public $modelClass = Customers::class;
}