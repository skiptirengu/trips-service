<?php

namespace app\modules\v1\controllers;

use app\models\Features;
use app\models\Places;
use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class FeaturesController extends BaseRestController
{
    /**
     * @inheritdoc
     */
    public $modelClass = Features::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create'], $actions['index'], $actions['update']);
        return $actions;
    }

    /**
     * @return Features
     * @throws ServerErrorHttpException
     */
    public function actionCreate()
    {
        $place = $this->findPlace();
        $model = new Features();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->validate()) {
            $model->link('place', $place);
            $model->properties = Json::decode($model->properties);
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $model->id], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }
        return $model;
    }

    /**
     * @return Places
     * @throws NotFoundHttpException
     */
    public function findPlace()
    {
        if (($model = Places::findOne(['id' => Yii::$app->getRequest()->get('placeId')])) === null) {
            throw new NotFoundHttpException('Not found.');
        }
        return $model;
    }
}