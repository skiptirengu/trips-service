<?php

namespace app\modules\v1\controllers;

use app\models\Places;

class PlacesController extends BaseRestController
{
    /**
     * @inheritdoc
     */
    public $modelClass = Places::class;
}