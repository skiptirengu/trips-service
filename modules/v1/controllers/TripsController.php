<?php

namespace app\modules\v1\controllers;

use app\models\Trips;

class TripsController extends BaseRestController
{
    /**
     * @inheritdoc
     */
    public $modelClass = Trips::class;
}