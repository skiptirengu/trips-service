<?php

namespace app\tests;

use Yii;
use yii\console\Application;
use yii\di\Container;

class TestCase extends \PHPUnit\Framework\TestCase
{
    protected function setUp()
    {
        parent::setUp();
        Yii::$app = new Application([
            'id' => 'trips-client-test',
            'basePath' => __DIR__
        ]);
        Yii::$container = new Container();
    }
}