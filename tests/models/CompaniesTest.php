<?php

namespace app\tests\models;

use app\models\Companies;
use app\tests\TestCase;

class CompaniesTest extends TestCase
{
    public function testValidationFail()
    {
        $model = new CompaniesStub();
        $this->assertFalse($model->validate());
        $this->assertTrue($model->hasErrors('name'));
        $this->assertFalse($model->isAttributeSafe('id'));
    }

    public function testValidationPass()
    {
        $model = new CompaniesStub();
        $this->assertTrue($model->load(['name' => 'Name'], ''));
        $this->assertTrue($model->validate());
        $this->assertFalse($model->hasErrors());
        $this->assertFalse($model->isAttributeSafe('id'));
    }
}

class CompaniesStub extends Companies
{
    public function attributes()
    {
        return ['id', 'name'];
    }
}
