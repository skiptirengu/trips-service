<?php

namespace app\tests\models;

use app\models\Customers;
use app\tests\TestCase;
use yii\validators\UniqueValidator;
use yii\validators\Validator;

class CustomersTest extends TestCase
{
    public function testValidationFail()
    {
        $model = new CustomersStub();
        $this->assertFalse($model->validate());
        $this->assertTrue($model->hasErrors());
        $this->assertFalse($model->isAttributeSafe('id'));
    }

    public function testValidationPass()
    {
        // skip unique validator (not in the mood to setup test db)
        Validator::$builtInValidators['unique'] = get_class($this->createMock(UniqueValidator::class));

        $model = new CustomersStub();
        $this->assertTrue($model->load(['name' => 'Name', 'email' => 'thiago@ogt.com'], ''));
        $this->assertTrue($model->validate());
        $this->assertFalse($model->hasErrors());
        $this->assertFalse($model->isAttributeSafe('id'));
    }
}

class CustomersStub extends Customers
{
    public function attributes()
    {
        return ['id', 'name', 'email'];
    }
}