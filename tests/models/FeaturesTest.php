<?php

namespace app\tests\models;

use app\models\Features;
use app\tests\TestCase;

class FeaturesTest extends TestCase
{
    public function testValidationFail()
    {
        $model = new FeaturesStub();
        $resource = fopen('php://stdout', '+r');
        $this->assertTrue($model->load([
            'latitude' => -91,
            'longitude' => 181,
            'properties' => $resource
        ], ''));
        $this->assertFalse($model->validate());
        $this->assertTrue($model->hasErrors('latitude'));
        $this->assertTrue($model->hasErrors('longitude'));
        $this->assertTrue($model->hasErrors('properties'));
        $this->assertFalse($model->isAttributeSafe('id'));
        $this->assertFalse($model->isAttributeSafe('place_id'));
        fclose($resource);
    }

    public function testValidationPass()
    {
        $model = new FeaturesStub();
        $this->assertTrue($model->load([
            'geometry' => ['type' => 'newType', 'coordinates' => [60, 170]],
            'properties' => ['prop1' => 'test'],

        ], ''));
        $this->assertSame(['prop1' => 'test'], $model->properties);
        $this->assertTrue($model->validate());
        $this->assertSame('newType', $model->type);
        $this->assertSame(60, $model->latitude);
        $this->assertSame(170, $model->longitude);
        $this->assertSame('{"prop1":"test"}', $model->properties);
        $this->assertFalse($model->isAttributeSafe('id'));
        $this->assertFalse($model->isAttributeSafe('place_id'));
        $this->assertFalse($model->hasErrors());

    }
}

class FeaturesStub extends Features
{
    public function attributes()
    {
        return [
            'id', 'type', 'place_id', 'properties', 'latitude', 'longitude'
        ];
    }
}
