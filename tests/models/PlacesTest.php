<?php

namespace app\tests\models;

use app\models\Places;
use app\tests\TestCase;

class PlacesTest extends TestCase
{
    public function testValidationFail()
    {
        $model = new CustomersStub();
        $this->assertFalse($model->validate());
        $this->assertTrue($model->hasErrors());
        $this->assertFalse($model->isAttributeSafe('id'));
    }

    public function testValidationPass()
    {
        $model = new PlacesStub();
        $this->assertTrue($model->load(['label' => 'Test'], ''));
        $this->assertTrue($model->validate());
        $this->assertFalse($model->hasErrors());
        $this->assertFalse($model->isAttributeSafe('id'));
    }
}

class PlacesStub extends Places
{
    public function attributes()
    {
        return ['id', 'label'];
    }
}