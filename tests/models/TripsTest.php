<?php

namespace app\tests\models;

use app\models\Trips;
use app\tests\TestCase;
use yii\validators\ExistValidator;
use yii\validators\Validator;

class TripsTest extends TestCase
{
    public function testValidationFail()
    {
        $model = new TripsStub();
        $this->assertFalse($model->validate());
        $this->assertTrue($model->hasErrors());
        $this->assertFalse($model->isAttributeSafe('company_id'));
        $this->assertFalse($model->isAttributeSafe('customer_id'));
        $this->assertTrue($model->isAttributeSafe('company'));
        $this->assertTrue($model->isAttributeSafe('customer'));
    }

    public function testValidationPass()
    {
        Validator::$builtInValidators['exist'] = get_class($this->createMock(ExistValidator::class));
        $model = new TripsStub();
        $this->assertTrue($model->load([
            'company' => 1,
            'customer' => 2,
            'from' => 3,
            'target' => 4,
            'cost' => 42.2,
            'departure' => '2017-06-21T16:00:00Z',
            'return' => '2017-07-01T08:00:00Z'
        ], ''));
        $this->assertSame(1, $model->company_id);
        $this->assertSame(2, $model->customer_id);
        $this->assertTrue($model->validate());
        $this->assertFalse($model->hasErrors());
    }
}

class TripsStub extends Trips
{
    public function attributes()
    {
        return ['departure', 'return', 'cost', 'company_id', 'customer_id', 'from', 'target'];
    }
}
