<?php

$localConfig = [];
if (is_file(__DIR__ . '/../config/local-bootstrap.php')) {
    $localConfig = require(__DIR__ . '/../config/local-bootstrap.php');
}

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = array_replace_recursive(require(__DIR__ . '/../config/web.php'), $localConfig);

(new yii\web\Application($config))->run();
